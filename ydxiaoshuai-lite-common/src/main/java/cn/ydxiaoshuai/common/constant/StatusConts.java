package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className StatusConts
 * @Description 状态常量
 * @Date 2020/5/15-17:54
 **/
public interface StatusConts {
    /**LOGO审核-通过 **/
    public static Integer LOGO_EXAMINE_APPROVED = 1;
    /**LOGO审核-拒绝 **/
    public static Integer LOGO_EXAMINE_REFUSE = 1;

    public static String COMMIT = "commit";
    public static String RUNNING = "running";
    public static String FAILED = "failed";
    public static String SUCCESS = "success";
}
