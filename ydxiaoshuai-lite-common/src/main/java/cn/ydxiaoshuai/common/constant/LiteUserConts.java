package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className LiteUserConts
 * @Description 小程序用户常量
 * @Date 2020/5/14-14:45
 **/
public class LiteUserConts {
    /**
     * 用户访问类型 网页(H5,公众号网页)
     **/
    public static final String USER_TYPE_WEB = "WEB";
    /**
     * 用户访问类型 小程序(目前只有微信小程序)
     **/
    public static final String USER_TYPE_LITE = "LITE";
    /**
     * 用户访问类型 APP(Android、iOS)
     **/
    public static final String USER_TYPE_APP = "APP";
}
