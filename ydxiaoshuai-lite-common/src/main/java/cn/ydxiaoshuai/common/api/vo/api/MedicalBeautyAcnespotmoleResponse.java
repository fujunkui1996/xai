package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className MedicalBeautyAcnespotmoleResponse
 * @Description 痘斑痣检测接口返回的对象
 * @Date 2020/4/9-15:49
 **/
@NoArgsConstructor
@Data
public class MedicalBeautyAcnespotmoleResponse {


    private int error_code;
    private String error_msg;
    private long log_id;
    private Integer timestamp;
    private Integer cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private Integer face_num;
        private List<FaceListBean> face_list;

        @NoArgsConstructor
        @Data
        public static class FaceListBean {

            private String face_token;
            private LocationBean location;
            private List<AcneListBean> acne_list;
            private List<SpeckleListBean> speckle_list;
            private List<MoleListBean> mole_list;

            @NoArgsConstructor
            @Data
            public static class LocationBean {

                private double left;
                private double top;
                private int width;
                private int height;
                private int degree;
            }

            @NoArgsConstructor
            @Data
            public static class AcneListBean {

                private double score;
                private double left;
                private double top;
                private double right;
                private double bottom;
            }
            @NoArgsConstructor
            @Data
            public static class SpeckleListBean {

                private double score;
                private double left;
                private double top;
                private double right;
                private double bottom;
            }

            @NoArgsConstructor
            @Data
            public static class MoleListBean {

                private double score;
                private double left;
                private double top;
                private double right;
                private double bottom;
            }
        }
    }
}
