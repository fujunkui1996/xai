package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceDriverVirtualResponseBean
 * @Description 响应参数
 * @Date 2021-05-12-14:18
 **/
@NoArgsConstructor
@Data
public class FaceDriverVirtualResponseBean {

    /**
     * error_code : 0
     * error_msg : SUCCESS
     * log_id : 8925201799965
     * timestamp : 1620800277
     * cached : 0
     * result : {"task_id":"v98mt3mAJLqs"}
     */

    private int error_code;
    private String error_msg;
    private long log_id;
    private int timestamp;
    private int cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        /**
         * task_id : v98mt3mAJLqs
         */
        private String task_id;
        private String status;
        private String video_url;
    }
}
