package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className FacePlantBean
 * @Description 幸运植物
 * @Date 2020/9/18-14:50
 **/
@NoArgsConstructor
@Data
public class FacePlantBean {

    /**
     * extData : {"template_name":"atom/plantsRecommend","template_id":"G699"}
     * commonData : {"feRoot":"https://mms-static.cdn.bcebos.com/graph/graphfe/static","os":"ios","sidList":"10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001","graphEnv":"wise","nativeSdk":false,"sf":0,"isNaView":0,"isHalfWap":0}
     * tplData : {"default":"faceFortune","faceLevel":{"title":"气质相同的花","plantsList":[{"name":"富贵竹","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E5%AF%8C%E8%B4%B5%E7%AB%B9.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e5%af%8c%e8%b4%b5%e7%ab%b9.png"},{"name":"水仙","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E6%B0%B4%E4%BB%99.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e6%b0%b4%e4%bb%99.png"},{"name":"薜荔","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E8%96%9C%E8%8D%94.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e8%96%9c%e8%8d%94.png"}]},"faceFortune":{"title":"幸运植物","plantsList":[{"name":"仙人掌","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%BB%99%E4%BA%BA%E6%8E%8C.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%bb%99%e4%ba%ba%e6%8e%8c.png"},{"name":"乳茄","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%B9%B3%E8%8C%84.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%b9%b3%e8%8c%84.png"},{"name":"独花兰","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E7%8B%AC%E8%8A%B1%E5%85%B0.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e7%8b%ac%e8%8a%b1%e5%85%b0.png"}]}}
     */

    private ExtDataBean extData;
    private CommonDataBean commonData;
    private TplDataBean tplData;

    @NoArgsConstructor
    @Data
    public static class ExtDataBean {
        /**
         * template_name : atom/plantsRecommend
         * template_id : G699
         */

        private String template_name;
        private String template_id;
    }

    @NoArgsConstructor
    @Data
    public static class CommonDataBean {
        /**
         * feRoot : https://mms-static.cdn.bcebos.com/graph/graphfe/static
         * os : ios
         * sidList : 10010_10125_10028_10003_10005_10104_10201_10040_10072_10062_10080_10190_10290_10390_10692_10704_10705_10301_10709_10802_10902_11006_10905_10911_11001
         * graphEnv : wise
         * nativeSdk : false
         * sf : 0
         * isNaView : 0
         * isHalfWap : 0
         */

        private String feRoot;
        private String os;
        private String sidList;
        private String graphEnv;
        private boolean nativeSdk;
        private int sf;
        private int isNaView;
        private int isHalfWap;
    }

    @NoArgsConstructor
    @Data
    public static class TplDataBean {
        /**
         * default : faceFortune
         * faceLevel : {"title":"气质相同的花","plantsList":[{"name":"富贵竹","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E5%AF%8C%E8%B4%B5%E7%AB%B9.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e5%af%8c%e8%b4%b5%e7%ab%b9.png"},{"name":"水仙","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E6%B0%B4%E4%BB%99.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e6%b0%b4%e4%bb%99.png"},{"name":"薜荔","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E8%96%9C%E8%8D%94.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e8%96%9c%e8%8d%94.png"}]}
         * faceFortune : {"title":"幸运植物","plantsList":[{"name":"仙人掌","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%BB%99%E4%BA%BA%E6%8E%8C.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%bb%99%e4%ba%ba%e6%8e%8c.png"},{"name":"乳茄","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%B9%B3%E8%8C%84.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%b9%b3%e8%8c%84.png"},{"name":"独花兰","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E7%8B%AC%E8%8A%B1%E5%85%B0.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e7%8b%ac%e8%8a%b1%e5%85%b0.png"}]}
         */

        private String defaultX;
        private FaceLevelBean faceLevel;
        private FaceFortuneBean faceFortune;

        @NoArgsConstructor
        @Data
        public static class FaceLevelBean {
            /**
             * title : 气质相同的花
             * plantsList : [{"name":"富贵竹","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E5%AF%8C%E8%B4%B5%E7%AB%B9.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e5%af%8c%e8%b4%b5%e7%ab%b9.png"},{"name":"水仙","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E6%B0%B4%E4%BB%99.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e6%b0%b4%e4%bb%99.png"},{"name":"薜荔","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E8%96%9C%E8%8D%94.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e8%96%9c%e8%8d%94.png"}]
             */

            private String title;
            private List<PlantsListBean> plantsList;

            @NoArgsConstructor
            @Data
            public static class PlantsListBean {
                /**
                 * name : 富贵竹
                 * imgUrl : https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E5%AF%8C%E8%B4%B5%E7%AB%B9.png
                 * jumpUrl : https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e5%af%8c%e8%b4%b5%e7%ab%b9.png
                 */

                private String name;
                private String imgUrl;
                private String jumpUrl;
            }
        }

        @NoArgsConstructor
        @Data
        public static class FaceFortuneBean {
            /**
             * title : 幸运植物
             * plantsList : [{"name":"仙人掌","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%BB%99%E4%BA%BA%E6%8E%8C.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%bb%99%e4%ba%ba%e6%8e%8c.png"},{"name":"乳茄","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%B9%B3%E8%8C%84.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%b9%b3%e8%8c%84.png"},{"name":"独花兰","imgUrl":"https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E7%8B%AC%E8%8A%B1%E5%85%B0.png","jumpUrl":"https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e7%8b%ac%e8%8a%b1%e5%85%b0.png"}]
             */

            private String title;
            private List<PlantsListBeanX> plantsList;

            @NoArgsConstructor
            @Data
            public static class PlantsListBeanX {
                /**
                 * name : 仙人掌
                 * imgUrl : https://mms-graph.cdn.bcebos.com/face/plantRecommend/%E4%BB%99%E4%BA%BA%E6%8E%8C.png
                 * jumpUrl : https://graph.baidu.com/api/proxy?mroute=activityGuide&f=plant&promotion_name=face_result_page&imgurl=https%3a%2f%2fmms-graph.cdn.bcebos.com%2fface%2fplantRecommend%2f%e4%bb%99%e4%ba%ba%e6%8e%8c.png
                 */

                private String name;
                private String imgUrl;
                private String jumpUrl;
            }
        }
    }
}
