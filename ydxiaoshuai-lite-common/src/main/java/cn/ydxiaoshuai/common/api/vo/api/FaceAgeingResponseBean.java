package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @Description 人脸变老返回的内容
 * @author 小帅丶
 * @className FaceAgeingResponseBean
 * @Date 2020/1/8-17:54
 **/
@Data
public class FaceAgeingResponseBean extends BaseBean {
    private Data data;
    @lombok.Data
    public static class Data{
        private String image_url;
        private String image_base64;
        private String similarity;
    }
    public FaceAgeingResponseBean success(String msg,String msg_zh, Data data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public FaceAgeingResponseBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public FaceAgeingResponseBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
