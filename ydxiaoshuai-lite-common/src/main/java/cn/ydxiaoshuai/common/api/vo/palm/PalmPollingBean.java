package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

import java.util.List;

/**
 * @Description 手部位置信息
 * @author 小帅丶
 * @className PalmPollingBean
 * @Date 2020/1/3-10:45
 **/
@Data
public class PalmPollingBean {

    /**
     * msg : success
     * code : 1
     * data : {"isLeft":false,"hand":{"x":[157,425],"y":[28,494]},"palm":{"score":0.9871429800987244,"x":[0,190],"y":[190,466]},"fingers":{"middle":{"score":0.9997792840003967,"x":[74,121],"y":[1,57]},"ring":{"score":0.9911106824874878,"x":[40,83],"y":[26,82]},"index":{"score":0.9991061091423035,"x":[120,165],"y":[29,84]},"little":{"score":0.9992062449455261,"x":[2,46],"y":[93,147]}},"imgUrl":"https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg","lines":{"LL":{"x":[168,161,160,157,154,149,145,142,134,127,119,112,106,102,98,94,91,90,90,92],"y":[252,257,259,261,264,268,272,275,283,292,301,311,321,329,339,351,362,374,387,401]},"WL":{"x":[163,157,154,152,136,129,124,122,118,113,109,105,101,97,93,88,84,79,74,66],"y":[256,259,260,261,270,274,276,278,281,283,286,289,292,294,297,301,304,308,312,319]},"AL":{"x":[125,114,106,97,91,82,76,72,65,62,54,50,46,42,37,32,28,22,17,5],"y":[254,260,264,268,271,275,278,279,282,283,286,287,289,290,292,293,294,296,297,300]}}}
     */
    /** 应答信息 */
    private String msg;
    /** 应答码 */
    private int code;
    /** 具体数据 */
    private DataBean data;

    @Data
    public static class DataBean {
        /**
         * isLeft : false
         * hand : {"x":[157,425],"y":[28,494]}
         * palm : {"score":0.9871429800987244,"x":[0,190],"y":[190,466]}
         * fingers : {"middle":{"score":0.9997792840003967,"x":[74,121],"y":[1,57]},"ring":{"score":0.9911106824874878,"x":[40,83],"y":[26,82]},"index":{"score":0.9991061091423035,"x":[120,165],"y":[29,84]},"little":{"score":0.9992062449455261,"x":[2,46],"y":[93,147]}}
         * imgUrl : https://bs-ai.ggwan.com/hand/product/7d3c4b748cbf42a3b4a5bf0ecd941565202001.jpg
         * lines : {"LL":{"x":[168,161,160,157,154,149,145,142,134,127,119,112,106,102,98,94,91,90,90,92],"y":[252,257,259,261,264,268,272,275,283,292,301,311,321,329,339,351,362,374,387,401]},"WL":{"x":[163,157,154,152,136,129,124,122,118,113,109,105,101,97,93,88,84,79,74,66],"y":[256,259,260,261,270,274,276,278,281,283,286,289,292,294,297,301,304,308,312,319]},"AL":{"x":[125,114,106,97,91,82,76,72,65,62,54,50,46,42,37,32,28,22,17,5],"y":[254,260,264,268,271,275,278,279,282,283,286,287,289,290,292,293,294,296,297,300]}}
         */

        private boolean isLeft;
        private HandBean hand;
        private PalmBean palm;
        private FingersBean fingers;
        private String imgUrl;
        private String handUrl;
        private LinesBean lines;

        @Data
        public static class HandBean {
            private List<Integer> x;
            private List<Integer> y;
        }

        @Data
        public static class PalmBean {
            /**
             * score : 0.9871429800987244
             * x : [0,190]
             * y : [190,466]
             */

            private double score;
            private List<Integer> x;
            private List<Integer> y;
        }

        @Data
        public static class FingersBean {
            /**
             * middle : {"score":0.9997792840003967,"x":[74,121],"y":[1,57]}
             * ring : {"score":0.9911106824874878,"x":[40,83],"y":[26,82]}
             * index : {"score":0.9991061091423035,"x":[120,165],"y":[29,84]}
             * little : {"score":0.9992062449455261,"x":[2,46],"y":[93,147]}
             */

            private MiddleBean middle;
            private RingBean ring;
            private IndexBean index;
            private LittleBean little;

            @Data
            public static class MiddleBean {
                /**
                 * score : 0.9997792840003967
                 * x : [74,121]
                 * y : [1,57]
                 */

                private double score;
                private List<Integer> x;
                private List<Integer> y;

            }

            @Data
            public static class RingBean {
                /**
                 * score : 0.9911106824874878
                 * x : [40,83]
                 * y : [26,82]
                 */

                private double score;
                private List<Integer> x;
                private List<Integer> y;

            }

            @Data
            public static class IndexBean {
                /**
                 * score : 0.9991061091423035
                 * x : [120,165]
                 * y : [29,84]
                 */

                private double score;
                private List<Integer> x;
                private List<Integer> y;

            }

            @Data
            public static class LittleBean {
                /**
                 * score : 0.9992062449455261
                 * x : [2,46]
                 * y : [93,147]
                 */

                private double score;
                private List<Integer> x;
                private List<Integer> y;

            }
        }

        @Data
        public static class LinesBean {
            /**
             * LL : {"x":[168,161,160,157,154,149,145,142,134,127,119,112,106,102,98,94,91,90,90,92],"y":[252,257,259,261,264,268,272,275,283,292,301,311,321,329,339,351,362,374,387,401]}
             * WL : {"x":[163,157,154,152,136,129,124,122,118,113,109,105,101,97,93,88,84,79,74,66],"y":[256,259,260,261,270,274,276,278,281,283,286,289,292,294,297,301,304,308,312,319]}
             * AL : {"x":[125,114,106,97,91,82,76,72,65,62,54,50,46,42,37,32,28,22,17,5],"y":[254,260,264,268,271,275,278,279,282,283,286,287,289,290,292,293,294,296,297,300]}
             */

            private LLBean LL;
            private WLBean WL;
            private ALBean AL;

            @Data
            public static class LLBean {
                private List<Integer> x;
                private List<Integer> y;

            }

            @Data
            public static class WLBean {
                private List<Integer> x;
                private List<Integer> y;

            }

            @Data
            public static class ALBean {
                private List<Integer> x;
                private List<Integer> y;

            }
        }
    }
}
