package cn.ydxiaoshuai.modules.liteuser.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 微信用户信息表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("liteuser_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="liteuser_info对象", description="微信用户信息表")
public class LiteUserInfo {
    
	/**主键ID*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键ID")
	private String id;
	/**用户ID 系统生成的*/
	@Excel(name = "用户ID", width = 15)
    @ApiModelProperty(value = "用户ID")
	private String userId;
	/**用户昵称*/
	@Excel(name = "用户昵称", width = 15)
    @ApiModelProperty(value = "用户昵称")
	private String nickName;
	/**用户性别*/
	@Excel(name = "用户性别", width = 15)
    @ApiModelProperty(value = "用户性别")
	private String gender;
	/**用户语言*/
	@Excel(name = "用户语言", width = 15)
    @ApiModelProperty(value = "用户语言")
	private String language;
	/**用户所在城市*/
	@Excel(name = "用户所在城市", width = 15)
    @ApiModelProperty(value = "用户所在城市")
	private String city;
	/**用户所在省份*/
	@Excel(name = "用户所在省份", width = 15)
    @ApiModelProperty(value = "用户所在省份")
	private String province;
	/**用户所在国家*/
	@Excel(name = "用户所在国家", width = 15)
    @ApiModelProperty(value = "用户所在国家")
	private String country;
	/**用户头像地址*/
	@Excel(name = "用户头像地址", width = 15)
    @ApiModelProperty(value = "用户头像地址")
	private String avatarUrl;
	/**用户openid 针对于小程序是唯一标识 微信会有*/
	@Excel(name = "用户openid", width = 15)
    @ApiModelProperty(value = "用户openid")
	private String openid;
	/**用户可用次数*/
	@Excel(name = "用户可用次数", width = 15)
    @ApiModelProperty(value = "用户可用次数")
	private Integer frequency;
	/**用户第一次授权时间*/
	@Excel(name = "用户第一次授权时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "用户第一次授权时间")
	private Date firstDate;
	/**用户第一次访问时间*/
	@Excel(name = "用户第一次访问时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "用户第一次访问时间")
	private Date visitDate;
	/**用户信息更新时间*/
	@Excel(name = "用户信息更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "用户信息更新时间")
	private Date updateDate;
	/**用户等级*/
	@Excel(name = "用户等级", width = 15)
    @ApiModelProperty(value = "用户等级")
	private String level;
	/**用户第一次访问时间到现在的天数 每天定时更新*/
	@Excel(name = "用户注册时长", width = 15)
    @ApiModelProperty(value = "用户注册时长")
	private Integer days;
	/**用户微信账号*/
	@Excel(name = "用户微信账号", width = 15)
    @ApiModelProperty(value = "用户微信账号")
	private String wxAccount;
	/**用户手机账号*/
	@Excel(name = "用户手机账号", width = 15)
    @ApiModelProperty(value = "用户手机账号")
	private String phoneNum;
	/**用户年龄*/
	@Excel(name = "用户年龄", width = 15)
    @ApiModelProperty(value = "用户年龄")
	private Integer age;
	/**用户积分 默认0*/
	@Excel(name = "用户积分 默认0", width = 15)
    @ApiModelProperty(value = "用户积分 默认0")
	private Integer integral;
	/**用户信息更新者名称*/
	@Excel(name = "用户信息更新者名称", width = 15)
    @ApiModelProperty(value = "用户信息更新者名称")
	private String updateUser;
	/**用户类型 1微信小程序 2支付宝小程序 3QQ小程序 4百度小程序  */
	@Excel(name = "用户类型", width = 15)
    @ApiModelProperty(value = "用户类型")
	private Integer userType;
	/**内部编号*/
	@Excel(name = "内部编号", width = 15)
	@ApiModelProperty(value = "内部编号")
	private String accountCode;
	/**创建人*/
	@Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新人*/
	@Excel(name = "更新人", width = 15)
    @ApiModelProperty(value = "更新人")
	private String updateBy;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;
}
