package cn.ydxiaoshuai.modules.weixin.po.wxlite;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

/**
 * @Description 微信授权绑定对象
 * @author 小帅丶
 * @className WXPage
 * @Date 2019/12/2-15:24
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WXBind extends BaseResponseBean {
    //具体返回内容
    private WXData data;
    @Data
    public static class WXData{
        /**用户积分 */
        private String integralTotal;
        /**用户SBSP唯一标识 */
        private String userId;
        /**微信唯一标识 */
        private String openid;
        /** 头像地址 */
        private String headimgurl;
        /** 昵称 */
        private String nickname;
        /** 备注 */
        private String remark;
        /** 用户绑定状态 0未绑定|未注册 1已绑定|已注册*/
        private Integer bind;
        /**用户类型 其他为空 小程序传递为 lite*/
        private String userType;
        /**内部编码*/
        private String account_code;

    }
    public WXBind success(String message) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        return this;
    }
    public WXBind success(String message, WXData data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public WXBind success(String message,Integer code, WXData data) {
        this.message = message;
        this.code = code;
        this.data = data;
        return this;
    }
    public WXBind unknow(String message,Integer code, WXData data) {
        this.message = message;
        this.code = code;
        this.data = data;
        return this;
    }
    public WXBind fail(String message, Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
    public WXBind error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}
