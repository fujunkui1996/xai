package cn.ydxiaoshuai.modules.weixin.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.weixin.entity.WeixinAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信公众账号表
 * @Author: 小帅丶
 * @Date:   2020-09-10
 * @Version: V1.0
 */
public interface WeixinAccountMapper extends BaseMapper<WeixinAccount> {

}
