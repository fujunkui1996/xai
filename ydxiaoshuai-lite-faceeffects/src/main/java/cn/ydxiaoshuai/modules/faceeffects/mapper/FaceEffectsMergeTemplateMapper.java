package cn.ydxiaoshuai.modules.faceeffects.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.faceeffects.entity.FaceEffectsMergeTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 人脸融合模板图表
 * @Author: 小帅丶
 * @Date:   2020-09-04
 * @Version: V1.0
 */
public interface FaceEffectsMergeTemplateMapper extends BaseMapper<FaceEffectsMergeTemplate> {

}
