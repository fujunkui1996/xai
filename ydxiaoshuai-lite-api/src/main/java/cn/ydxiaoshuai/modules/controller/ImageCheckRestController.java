package cn.ydxiaoshuai.modules.controller;

import cn.hutool.core.util.IdUtil;
import cn.ydxiaoshuai.common.api.vo.weixin.WeiXinConts;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.modules.util.WeiXinSecurityUtil;
import cn.ydxiaoshuai.modules.weixin.po.WXAccessToken;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import cn.ydxiaoshuai.common.util.oConvertUtils;


/**
 * @author 小帅丶
 * @className ImageCheckRestController
 * @Description 图片检查
 * @Date 2020/9/14-12:10
 **/
@Slf4j
@Controller
@RequestMapping(value = "/rest/check")
@Scope("prototype")
@Api(tags = "小程序-图像违规检测API")
public class ImageCheckRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 图片过滤检测
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/imgcheck", method = {RequestMethod.POST})
    public ResponseEntity<Object> checkPic(@RequestParam(value = "file") MultipartFile file) {
        //公众号编码
        String account_code = ServletRequestUtils.getStringParameter(request, "account_code", "YDXSAI");
        log.info("访问的account_code：" + account_code + ",访问的ip：" + request.getRemoteAddr() + ",访问的userId:" + userId);
        param = "userId=" + userId + ",account_code=" + account_code;
        WXAccessToken bean = new WXAccessToken();
        try {
            startTime = System.currentTimeMillis();
            if (account_code.equals("XSYDZX") || account_code.equals("YDXSAI") || account_code.equals("ZFBAI")) {
                WeiXinConts weiXinConts = (WeiXinConts) redisUtil.get(account_code);
                bean = WeiXinSecurityUtil.checkImg(file, weiXinConts);
            } else {
                bean.setErrcode(505);
                bean.setErrmsg("code错误");
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            bean.setErrcode(500);
            bean.setErrmsg("system错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        //响应的内容
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(System.currentTimeMillis() + "-" + IdUtil.fastSimpleUUID(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.IMG_CHECK, userId, userAgent);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

    /**
     * 图片过滤检测
     *
     * @param file 图片文件
     * @param access_token 微信token
     * @return
     */
    @RequestMapping(value = "/general_file", method = {RequestMethod.POST})
    public ResponseEntity<Object> checkPicGeneral(@RequestParam(value = "file") MultipartFile file,String access_token) {
        log.info("访问的ip：" + ip);
        WXAccessToken bean = new WXAccessToken();
        try {
            startTime = System.currentTimeMillis();
            if (oConvertUtils.isEmpty(access_token)) {
                bean.setErrcode(410101);
                bean.setErrmsg("接口参数缺失，请检查access_token是否填写");
            } else {
                bean = WeiXinSecurityUtil.checkImg(file,access_token);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info(requestURI + e.getMessage());
            bean.setErrcode(500);
            bean.setErrmsg("system错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        //响应的内容
        beanStr = JSON.toJSONString(bean);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

    /**
     * 图片过滤检测
     *
     * @param url 图片公网URL
     * @param access_token 微信token
     * @return
     */
    @RequestMapping(value = "/general_url", method = {RequestMethod.POST})
    public ResponseEntity<Object> checkPicGeneral(String url,String access_token) {
        log.info("访问的ip：" + ip);
        WXAccessToken bean = new WXAccessToken();
        try {
            startTime = System.currentTimeMillis();
            if (oConvertUtils.isEmpty(access_token)) {
                bean.setErrcode(410101);
                bean.setErrmsg("接口参数缺失，请检查access_token是否填写");
            }else if(oConvertUtils.isEmpty(url)){
                bean.setErrcode(410101);
                bean.setErrmsg("接口参数缺失，请检查url是否填写");
            } else {
                bean = WeiXinSecurityUtil.checkImg(url,access_token);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info(requestURI + e.getMessage());
            bean.setErrcode(500);
            bean.setErrmsg("system错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        //响应的内容
        beanStr = JSON.toJSONString(bean);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }
}
